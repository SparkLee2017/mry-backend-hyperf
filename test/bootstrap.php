<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
ini_set('display_errors', 'on');
ini_set('display_startup_errors', 'on');

error_reporting(E_ALL);
date_default_timezone_set('Asia/Shanghai');

! defined('BASE_PATH') && define('BASE_PATH', dirname(__DIR__, 1));
! defined('SWOOLE_HOOK_FLAGS') && define('SWOOLE_HOOK_FLAGS', SWOOLE_HOOK_ALL);

Swoole\Runtime::enableCoroutine(true);

require BASE_PATH . '/vendor/autoload.php';

Hyperf\Di\ClassLoader::init();

$container = require BASE_PATH . '/config/container.php';

$container->get(Hyperf\Contract\ApplicationInterface::class);

// 本项目依赖 composer 扩展 "tangwei/dto" 将 http 请求参数自动转化为 Controller 方法中的 DTO 参数对象。而 "tangwei/dto" 是通过监听 Hyperf\Framework\Event\BeforeServerStart 和
// Hyperf\Server\Event\MainCoroutineServerStart 这两个事件（参见：Hyperf\DTO\BeforeServerListener::listen()）来插入 DTO 自动转化逻辑代码的，但是在执行 phpunit 单元测试的流程中这
// 两个事件貌似都没有被触发，为了让 DTO 自动转化机制生效，故在此处手动触发一下 BeforeServerStart 事件。
$container->get(Psr\EventDispatcher\EventDispatcherInterface::class)->dispatch(new Hyperf\Framework\Event\BeforeServerStart('http'));
