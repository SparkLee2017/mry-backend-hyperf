<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace HyperfTest\Unit\Core\Common\Domain\User;

use App\Core\Common\Domain\User\Role;
use App\Core\Common\Domain\User\User;
use App\Core\Member\Domain\Member;
use App\Core\Tenant\Domain\Tenant;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
class UserTest extends TestCase
{
    public function test_should_create_human_user()
    {
        $memberId = Member::newMemberId();
        $tenantId = Tenant::newTenantId();
        $role = Role::TenantAdmin;
        $user = User::humanUser($memberId, 'Alex', $tenantId, $role);
        $this->assertSame($user->getMemberId(), $memberId);
        $this->assertSame($user->getTenantId(), $tenantId);
        $this->assertSame($user->getRole(), $role);
    }

    public function test_should_fail_to_create_human_user_with_incomplete_info()
    {
        $this->expectException(InvalidArgumentException::class);
        User::humanUser('', 'Alex', Tenant::newTenantId(), Role::TenantAdmin);

        $this->expectException(InvalidArgumentException::class);
        User::humanUser(Member::newMemberId(), '', Tenant::newTenantId(), Role::TenantAdmin);

        $this->expectException(InvalidArgumentException::class);
        User::humanUser(Member::newMemberId(), 'Alex', '', Role::TenantAdmin);
    }

    public function test_should_fail_to_create_robot_user_with_incomplete_info()
    {
        $this->expectException(InvalidArgumentException::class);
        User::robotUser('');
    }

    public function test_should_create_tenant_admin()
    {
        $memberId = Member::newMemberId();
        $tenantId = Tenant::newTenantId();
        $user = User::humanUser($memberId, 'Alex', $tenantId, Role::TenantAdmin);
        $this->assertTrue($user->isLoggedIn());
    }
}
