<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace HyperfTest\Unit\Core\Common\Domain;

use App\Core\Common\Domain\AggregateRoot;
use App\Core\Common\Domain\Event\DomainEvent;
use App\Core\Common\Domain\OpsLog;
use App\Core\Common\Domain\User\Role;
use App\Core\Common\Domain\User\User;
use App\Core\Common\Utils\SnowflakeIdGenerator;
use App\Core\Member\Domain\Member;
use App\Core\Tenant\Domain\Tenant;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @coversNothing
 */
class AggregateRootTest extends TestCase
{
    private User $testUser;

    protected function setUp(): void
    {
        parent::setUp();

        $this->testUser = User::humanUser(Member::newMemberId(), 'memberName', Tenant::newTenantId(), Role::TenantAdmin);
    }

    public function test_should_create()
    {
        $id = 'Test' . SnowflakeIdGenerator::newSnowflakeId();

        $aggregate = TestAggregate::fromIdUser($id, $this->testUser);
        $this->assertSame($id, $aggregate->getId());
        $this->assertSame($this->testUser->getTenantId(), $aggregate->getTenantId());

        $this->assertSame($this->testUser->getMemberId(), $aggregate->getCreatedBy());
        $this->assertSame($this->testUser->getName(), $aggregate->getCreator());
        $this->assertLessThanOrEqual(Carbon::now(), $aggregate->getCreatedAt());

        $this->assertNull($aggregate->getEvents());
        $this->assertNull($aggregate->getOpsLogs());

        $this->assertSame($id, $aggregate->getIdentifier());
    }

    public function test_should_raise_event()
    {
        $id = 'Test' . SnowflakeIdGenerator::newSnowflakeId();
        $aggregate = TestAggregate::fromIdUser($id, $this->testUser);
        $event = new TestDomainEvent();
        $aggregate->raiseEvent($event);
        $this->assertSame($event, $aggregate->getEvents()->get(0));
    }

    public function test_should_clear_events()
    {
        $id = 'Test' . SnowflakeIdGenerator::newSnowflakeId();
        $aggregate = TestAggregate::fromIdUser($id, $this->testUser);
        $event = new TestDomainEvent();
        $aggregate->raiseEvent($event);
        $aggregate->clearEvents();
        $this->assertNull($aggregate->getEvents());
    }

    public function test_should_add_ops_log()
    {
        $id = 'Test' . SnowflakeIdGenerator::newSnowflakeId();
        $aggregate = TestAggregate::fromIdUser($id, $this->testUser);
        $opsLog = 'Hello ops logs';
        $aggregate->addOpsLog($opsLog, $this->testUser);

        /** @var OpsLog $first */
        $first = $aggregate->getOpsLogs()[0];
        $this->assertSame($opsLog, $first->getNote());
    }

    public function test_should_slice_ops_logs_if_too_much()
    {
        $id = 'Test' . SnowflakeIdGenerator::newSnowflakeId();
        $aggregate = TestAggregate::fromIdUser($id, $this->testUser);
        $range = range(0, AggregateRoot::MAX_OPS_LOG_SIZE);
        array_walk($range, function ($index) use ($aggregate) {
            $opsLog = 'Hello ops logs';
            $aggregate->addOpsLog($opsLog, $this->testUser);
        });
        $lastLog = 'last ops log';
        $aggregate->addOpsLog($lastLog, $this->testUser);
        $this->assertCount(AggregateRoot::MAX_OPS_LOG_SIZE, $aggregate->getOpsLogs());

        /** @var OpsLog $opsLog */
        $opsLog = $aggregate->getOpsLogs()->last();
        $this->assertSame($lastLog, $opsLog->getNote());
    }
}

class TestAggregate extends AggregateRoot {}

class TestDomainEvent extends DomainEvent {}
