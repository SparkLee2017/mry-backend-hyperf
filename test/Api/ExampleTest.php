<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace HyperfTest\Api;

use HyperfTest\HttpTestCase;

/**
 * @internal
 * @coversNothing
 */
class ExampleTest extends HttpTestCase
{
    public function test_example()
    {
        $this->assertTrue(true);
        $this->assertTrue(is_array($this->get('/')));
    }
}
