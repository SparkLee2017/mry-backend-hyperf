<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace HyperfTest\Api\Core\Verification;

use HyperfTest\HttpTestCase;

/**
 * @internal
 * @coversNothing
 */
class VerificationControllerApiTest extends HttpTestCase
{
    public function test_should_create_verification_code_for_register()
    {
        $result = $this->json('verification-codes/for-register', ['mobileOrEmail' => '18973832617']);

        $this->assertSame(['id' => '111'], $result);
    }
}
