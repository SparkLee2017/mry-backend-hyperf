import http from 'k6/http';
import {check, group, sleep} from 'k6';

export const options = {
    thresholds: {                          // API 测试通过的断言条件
        checks: ['rate==1'],               // 100% 的 check 全部通过
        http_req_duration: ['p(95)<1000'], // 95% 的请求时延在 1s 以内
    },
};

export default function () {
    group('hyperf', (_) => {
        index();
    });
    return 0;
}

function index() {
    // const url = 'http://127.0.0.1:9501/';
    const url = 'http://localhost:9501/apps/1';
    check(http.get(url), {
        'index response ok': (res) => {
            // console.log(res.json());
            // return (res.status === 200) && (res.json().method === 'GET');
            return (res.status === 200);
        },
    });
}