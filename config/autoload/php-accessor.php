<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
$appEnv = env('APP_ENV', 'dev');
$genMeta = $appEnv == 'dev' ? 'yes' : 'no';

return [
    'proxy_root_directory' => BASE_PATH . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . 'php-accessor',
    'gen_meta' => $genMeta,
    'gen_proxy' => 'yes',
    'log_level' => \Psr\Log\LogLevel::INFO,
];
