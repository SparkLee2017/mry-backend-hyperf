<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Model;

use Hyperf\DbConnection\Model\Model as BaseModel;
use Hyperf\ModelCache\Cacheable;
use Hyperf\ModelCache\CacheableInterface;

abstract class Model extends BaseModel implements CacheableInterface
{
    use Cacheable;
}
