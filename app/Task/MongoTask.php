<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Task;

use Hyperf\Task\Annotation\Task;
use MongoDB\Client;
use MongoDB\Driver\BulkWrite;
use MongoDB\Driver\Exception\Exception;
use MongoDB\Driver\Manager;
use MongoDB\Driver\Query;
use MongoDB\Driver\WriteConcern;

class MongoTask
{
    public Manager $manager;
    private Client $client;

    public function __construct()
    {
        $this->manager = new Manager('mongodb://mongo:27017', []);
        $this->client = new Client('mongodb://mongo:27017');
    }

    #[Task]
    public function insert(string $namespace, array $document): ?int
    {
        $writeConcern = new WriteConcern(WriteConcern::MAJORITY, 1000);
        $bulk = new BulkWrite();
        $bulk->insert($document);

        $result = $this->manager()->executeBulkWrite($namespace, $bulk, $writeConcern);
        return $result->getUpsertedCount();
    }

    public function insert2()
    {
        $collection = $this->client->foo_db->foo_collection;
        $insertOneResult = $collection->insertOne([
            'name' => 'liwei',
            'age' => 35,
            'kind' => 'nice',
        ]);
        var_dump($insertOneResult->getInsertedId(), $insertOneResult->getInsertedCount());
    }

    /**
     * @throws Exception
     */
    #[Task]
    public function query(string $namespace, array $filter = [], array $options = []): array
    {
        $query = new Query($filter, $options);
        $cursor = $this->manager()->executeQuery($namespace, $query);
        return $cursor->toArray();
    }

    protected function manager(): Manager
    {
        return $this->manager;
    }
}
