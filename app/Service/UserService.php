<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Service;

use Hyperf\Config\Annotation\Value;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Logger\LoggerFactory;

use function Hyperf\Config\config;

class UserService implements UserServiceInterface
{
    #[Inject]
    private LoggerFactory $loggerFactory;

    #[Value(key: 'databases.default.pool.heartbeat')]
    private int $dbHeartbeat;

    public function greeting(): void
    {
        $log = $this->loggerFactory->get('user.service');
        $log->warning('hi, handsome boy.');
        $log->info('database info', ['heartbeat' => $this->dbHeartbeat]);
        $log->critical('db heartbeat', ['hb' => config('databases.default.pool.heartbeat')]);
    }
}
