<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Controller;

use App\Common\Controller\AbstractController;
use App\Service\UserService;
use App\Service\UserServiceInterface;
use Hyperf\Cache\Cache;
use Hyperf\Config\Annotation\Value;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Logger\LoggerFactory;
use Psr\Log\LoggerInterface;

use function Hyperf\Support\make;

class IndexController extends AbstractController
{
    protected LoggerInterface $logger;

    #[Inject(lazy: true)]
    private UserServiceInterface $userService;

    #[Inject]
    private Cache $cache;

    #[Value(key: 'logger.default.formatter.constructor.dateFormat')]
    private string $loggerDateFormat;

    public function __construct(LoggerFactory $loggerFactory)
    {
        $this->logger = $loggerFactory->get(__CLASS__);
    }

    public function index()
    {
        $user = $this->request->input('user', 'Hyperf');
        $method = $this->request->getMethod();

        $this->logger->alert('what the fuck', ['foo' => 'bar']);

        $this->userService->greeting();

        $this->cache->set('app', 'hyperf');

        $us = make(UserService::class);
        $us->greeting();

        return [
            'method' => $method,
            'message' => "Hello {$user}.",
            'loggerDateFormat' => $this->loggerDateFormat,
            'config' => \Hyperf\Config\config('logger'),
        ];
    }
}
