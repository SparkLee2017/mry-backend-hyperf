<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Common\Exception;

use RuntimeException;

final class MryException extends RuntimeException
{
    private readonly ErrorCode $errCode;

    private readonly array $data;

    private readonly string $userMessage;

    public function getErrCode(): ErrorCode
    {
        return $this->errCode;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getUserMessage(): string
    {
        return $this->userMessage;
    }

    public static function fromErrorCode(ErrorCode $code, string $userMessage): MryException
    {
        $ex = new MryException();
        $ex->errCode = $code;
        $ex->userMessage = $userMessage;
        $ex->message = $ex->message($userMessage);
        return $ex;
    }

    public static function fromErrorCodeWithData(ErrorCode $code, string $userMessage, array $data): MryException
    {
        $ex = self::fromErrorCode($code, $userMessage);
        $ex->data = array_merge($ex->data, $data);
        return $ex;
    }

    public static function fromErrorCodeWithKV(ErrorCode $code, string $userMessage, string $key, mixed $value): MryException
    {
        $ex = self::fromErrorCode($code, $userMessage);
        $ex->addData($key, $value);
        return $ex;
    }

    public static function requestValidationExceptionWithData(array $data): MryException
    {
        return self::fromErrorCodeWithData(ErrorCode::RequestValidationFailed, '请求数据验证失败。', $data);
    }

    public static function requestValidationExceptionWithKV(string $key, mixed $value): MryException
    {
        return self::fromErrorCodeWithKV(ErrorCode::RequestValidationFailed, '请求数据验证失败。', $key, $value);
    }

    public static function requestValidationExceptionWithMsg(string $msg): MryException
    {
        return self::fromErrorCode(ErrorCode::RequestValidationFailed, $msg);
    }

    public static function accessDeniedException(): MryException
    {
        return self::fromErrorCode(ErrorCode::AccessDenied, '权限不足。');
    }

    public static function accessDeniedExceptionWithMsg(string $msg): MryException
    {
        return self::fromErrorCode(ErrorCode::AccessDenied, $msg);
    }

    public static function authenticationException(): MryException
    {
        return self::fromErrorCode(ErrorCode::AuthenticationFailed, '登录失败。');
    }

    public function addData(string $key, mixed $value): void
    {
        $this->data[$key] = $value;
        $this->message = $this->message($this->userMessage);
    }

    private function message(string $userMessage): string
    {
        $msg = sprintf('[%s]', $this->errCode->name);
        $userMessage && $msg .= $userMessage;
        $this->data && $msg .= 'Data: ' . json_encode($this->data);
        return $msg;
    }
}
