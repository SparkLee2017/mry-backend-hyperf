<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Common\Exception;

// 400：请求验证错误，请求中资源所属关系错误等
// 401：认证错误
// 403：权限不够
// 404：资源未找到
// 409：业务异常
// 426：套餐检查失败
// 429: 请求过多
// 500：系统错误
enum ErrorCode
{
    // 400
    case UserNotCurrentMember;
    case QrNotBelongToApp;
    case PasswordConfirmNotMatch;
    case ApprovalPermissionNotAllowed;
    case ModifyPermissionNotAllowed;
    case ControlPermissionNotAllowed;
    case OperationPermissionNotAllowed;
    case RequestValidationFailed;

    // 401
    case AuthenticationFailed;

    // 403
    case AccessDenied;
    case WrongTenant;

    // 404
    case PageNotFound;
    case ControlNotFound;

    // 409
    case PlateQrNotMatch;
    case MemberAlreadyLocked;
    case VerificationCodeCountOverflow;
    case VerificationCodeCheckFailed;

    // 426
    case MemberCountLimitReached;
    case ControlTypesNotAllowed;

    // 429
    case TooManyRequest;

    // 500
    case SystemError;

    public function getStatus(): int
    {
        return match ($this) {
            // 400
            self::UserNotCurrentMember,
            self::QrNotBelongToApp,
            self::PasswordConfirmNotMatch,
            self::ApprovalPermissionNotAllowed,
            self::ModifyPermissionNotAllowed,
            self::ControlPermissionNotAllowed,
            self::OperationPermissionNotAllowed,
            self::RequestValidationFailed => 400,
            // 401
            self::AuthenticationFailed => 401,
            // 403
            self::AccessDenied,
            self::WrongTenant => 403,
            // 404
            self::PageNotFound,
            self::ControlNotFound => 404,
            // 409
            self::PlateQrNotMatch,
            self::MemberAlreadyLocked,
            self::VerificationCodeCountOverflow,
            self::VerificationCodeCheckFailed => 409,
            // 426
            self::MemberCountLimitReached,
            self::ControlTypesNotAllowed => 426,
            // 429
            self::TooManyRequest => 429,
            // 500
            self::SystemError => 500,
        };
    }
}
