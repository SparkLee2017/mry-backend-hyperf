<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Common\Exception;

use Carbon\Carbon;

class Error
{
    private ErrorCode $code;

    private string $message;

    private string $userMessage;

    private int $status;

    private string $path;

    private Carbon $timestamp;

    private string $traceId;

    private array $data;

    public static function fromMryException(MryException $ex, string $path, string $traceId): Error
    {
        $err = new Error();
        $errorCode = $ex->getCode();
        return $err;
    }
}
