<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Common\Domain\User;

use App\Core\Common\Exception\IllegalStateException;
use App\Core\Common\Exception\MryException;
use App\Core\Common\Utils\CommonUtils;

final class User
{
    private function __construct(
        private readonly ?string $memberId,
        private readonly ?string $name,
        private readonly string $tenantId,
        private readonly Role $role,
    ) {}

    public static function humanUser(string $memberId, string $name, string $tenantId, Role $role): User
    {
        CommonUtils::requireNonBlank($memberId, 'MemberId must not be blank.');
        CommonUtils::requireNonBlank($name, 'Name must not be blank.');
        CommonUtils::requireNonBlank($tenantId, 'TenantId must not be blank.');

        if ($role == Role::Robot) {
            throw new IllegalStateException('Human user should not have ROBOT role.');
        }

        return new User($memberId, $name, $tenantId, $role);
    }

    public static function robotUser(string $tenantId): User
    {
        CommonUtils::requireNonBlank($tenantId, 'TenantId must not be blank.');

        return new User(null, null, $tenantId, Role::Robot);
    }

    public function isLoggedIn(): bool
    {
        return $this->internalIsLoggedIn();
    }

    public function checkIsLoggedIn(): void
    {
        $this->internalCheckLoggedIn();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getMemberId(): string
    {
        return $this->memberId;
    }

    public function getTenantId(): string
    {
        return $this->tenantId;
    }

    public function getRole(): Role
    {
        return $this->role;
    }

    private function internalIsLoggedIn(): bool
    {
        return $this->tenantId && ! is_null($this->role);
    }

    private function internalCheckLoggedIn(): void
    {
        if (! $this->internalIsLoggedIn()) {
            throw MryException::authenticationException();
        }
    }
}
