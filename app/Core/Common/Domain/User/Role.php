<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Common\Domain\User;

enum Role: string
{
    case TenantAdmin = '系统管理员';
    case TenantMember = '普通成员';
    case Robot = 'API账号';

    public function getRoleName(): string
    {
        return $this->value;
    }
}
