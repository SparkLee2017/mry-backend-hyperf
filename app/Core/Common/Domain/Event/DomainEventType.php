<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Common\Domain\Event;

enum DomainEventType
{
    case TenantCreated;
    case AppCreated;
    case AppDeleted;
    case AppCreatedFromTemplate;
    case PageChangedToSubmitPerInstance;
    case PageChangedToSubmitPerMember;
    case AttributesCreated;
    case AttributesDeleted;
    case ControlsDeleted;
    case ControlOptionsDeleted;
    case SubmissionDeleted;
    case SubmissionCreated;
    case SubmissionUpdated;
    case SubmissionApproved;
    case MemberCreated;
    case MemberDeleted;
    case MemberNameChanged;
    case MemberDepartmentsChanged;
    case GroupCreated;
    case GroupDeactivated;
    case GroupActivated;
    case GroupDeleted;
    case GroupManagersChanged;
    case PagesDeleted;
    case QrBaseSettingUpdated;
    case QrCreated;
    case QrRenamed;
    case QrMarkedAsTemplate;
    case QrUnmarkedAsTemplate;
    case PlateBound;
    case PlateUnbound;
    case QrDeleted;
    case QrCustomIdUpdated;
    case QrActivated;
    case QrDeactivated;
    case QrCirculationStatusChanged;
    case QrGroupChanged;
    case QrPlateReset;
    case QrAttributesUpdated;
    case QrDescriptionUpdated;
    case QrGeolocationUpdated;
    case QrHeaderImageUpdated;
    case PlateBatchDeleted;
    case PlateBatchCreated;
    case TenantSubdomainUpdated;
    case TenantActivated;
    case TenantBaseSettingUpdated;
    case TenantInvoiceTitleUpdated;
    case TenantResourceUsageUpdated;
    case TenantPlanUpdated;
    case TenantSubdomainReadyStatusUpdated;
    case TenantOrderApplied;
    case TenantDeactivated;
    case OrderCreated;
    case OrderWxPayUpdated;
    case OrderWxTransferUpdated;
    case OrderBankTransferUpdated;
    case OrderDeliveryUpdated;
    case OrderInvoiceRequested;
    case OrderInvoiceIssued;
    case OrderRefundUpdated;
    case AssignmentPlanDeleted;
    case DepartmentCreated;
    case DepartmentDeleted;
    case DepartmentManagersChanged;
    case DepartmentRenamed;
    case MemberAddedToDepartment;
    case MemberRemovedFromDepartment;
    case GroupSyncEnabled;
    case DepartmentHierarchyChanged;
    case AssignmentCreated;
}
