<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Common\Domain\Event;

use App\Core\Common\Domain\AggregateRoot;
use Carbon\Carbon;

/**
 * DomainEvent既要保证能支持MongoDB的序列化/反序列化，又要能够通过json序列化/反序列化（因为要发送到Redis）.
 */
abstract class DomainEvent
{
    private string $id; // 事件ID，不能为空

    private string $arTenantId; // 事件对应的租户ID，不能为空

    private string $arId; // 事件对应的聚合根ID，不能为空

    private DomainEventType $type; // 事件类型

    private DomainEventStatus $status; // 状态

    private int $publishedCount; // 已经发布的次数，无论成功与否

    private int $consumedCount; // 已经被消费的次数，无论成功与否

    private string $raisedBy; // 引发该事件的memberId

    private Carbon $raisedAt; // 事件产生时间

    public function setArInfo(AggregateRoot $ar): void
    {
        $this->arTenantId = $ar->getTenantId();
        $this->arId = $ar->getId();
    }
}
