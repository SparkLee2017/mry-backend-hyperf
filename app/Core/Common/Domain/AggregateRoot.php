<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Common\Domain;

use App\Core\Common\Domain\Event\DomainEvent;
use App\Core\Common\Domain\User\User;
use App\Core\Common\Utils\CommonUtils;
use App\Core\Common\Utils\Identified;
use Carbon\Carbon;
use Hyperf\Collection\Collection;

abstract class AggregateRoot implements Identified
{
    public const MAX_OPS_LOG_SIZE = 20;

    private string $id; // 通过Snowflake算法生成

    private string $tenantId; // 在多租户下，所有聚合根都需要一个tenantId来对应某个租户

    private Carbon $createdAt; // 创建时间

    private string $createdBy; // 创建人的MemberId

    private string $creator; // 创建人姓名

    private Carbon $updatedAt; // 更新时间

    private string $updatedBy; // 更新人MemberId

    private string $updater; // 更新人姓名

    /** @var ?Collection<DomainEvent> */
    private ?Collection $events = null; // 领域事件列表，用于临时存放完成某个业务流程中所发出的事件，会被BaseRepository保存到事件表中

    /** @var ?Collection<OpsLog> */
    private ?Collection $opsLogs = null; // 操作日志

    public static function fromIdUser(string $id, User $user): static
    {
        CommonUtils::requireNonBlank($id, 'ID must not be blank.');
        CommonUtils::requireNonBlank($user->getTenantId(), 'Tenant ID must not be blank.');

        $root = new static();
        $root->id = $id;
        $root->tenantId = $user->getTenantId();
        $root->createdAt = Carbon::now();
        $root->createdBy = $user->getMemberId();
        $root->creator = $user->getName();
        return $root;
    }

    public static function fromIdTenantIdUser(string $id, string $tenantId, User $user): static
    {
        CommonUtils::requireNonBlank($id, 'AR ID must not be blank.');
        CommonUtils::requireNonBlank($tenantId, 'Tenant ID must not be blank.');

        $root = new static();
        $root->id = $id;
        $root->tenantId = $tenantId;
        $root->createdAt = Carbon::now();
        $root->createdBy = $user->getMemberId();
        $root->creator = $user->getName();
        return $root;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTenantId(): string
    {
        return $this->tenantId;
    }

    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    public function getCreator(): string
    {
        return $this->creator;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): Carbon
    {
        return $this->updatedAt;
    }

    public function getUpdatedBy(): string
    {
        return $this->updatedBy;
    }

    public function getUpdater(): string
    {
        return $this->updater;
    }

    /**
     * @return ?Collection<OpsLog>
     */
    public function getOpsLogs(): ?Collection
    {
        return $this->opsLogs;
    }

    /**
     * @return ?Collection<DomainEvent>
     */
    public function getEvents(): ?Collection
    {
        return $this->events;
    }

    public function addOpsLog(string $note, User $user): void
    {
        if ($user->isLoggedIn()) {
            $log = new OpsLog(optAt: Carbon::now(), optBy: $user->getMemberId(), obn: $user->getName(), note: $note);
            $opsLogs = $this->allOpsLogs();

            $opsLogs->push($log);
            if ($opsLogs->count() > self::MAX_OPS_LOG_SIZE) { // 最多保留最近n条
                $opsLogs->shift();
            }

            $this->updatedAt = Carbon::now();
            $this->updatedBy = $user->getMemberId();
            $this->updater = $user->getName();
        }
    }

    public function clearEvents(): void
    {
        $this->events = null;
    }

    public function getIdentifier(): string
    {
        return $this->id;
    }

    public function raiseEvent(DomainEvent $event): void
    {
        $event->setArInfo($this);
        $this->allEvents()->push($event);
    }

    /**
     * @return Collection<OpsLog>
     */
    private function allOpsLogs(): Collection
    {
        if (is_null($this->opsLogs)) {
            $this->opsLogs = new Collection();
        }

        return $this->opsLogs;
    }

    /**
     * @return Collection<DomainEvent>
     */
    private function allEvents(): Collection
    {
        if (is_null($this->events)) {
            $this->events = new Collection();
        }

        return $this->events;
    }
}
