<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Common\Domain;

use Carbon\Carbon;

class OpsLog
{
    public function __construct(
        private readonly Carbon $optAt,
        private readonly string $optBy,
        private readonly string $obn, // operatedByName
        private readonly string $note
    ) {}

    public function getOperatedAt(): Carbon
    {
        return $this->optAt;
    }

    public function getOperatedBy(): string
    {
        return $this->optBy;
    }

    public function getOperatedByName(): string
    {
        return $this->obn;
    }

    public function getNote(): string
    {
        return $this->note;
    }
}
