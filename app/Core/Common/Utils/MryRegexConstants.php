<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Common\Utils;

class MryRegexConstants
{
    public const MOBILE_PATTERN = '/^[1]([3-9])[0-9]{9}$/';

    public const EMAIL_PATTERN = '/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$/';

    public const PASSWORD_PATTERN = '/^[A-Za-z\\d!@#$%^&*()_+]{8,20}$/';

    public const VERIFICATION_CODE_PATTERN = '/^[0-9]{6}$/';

    public const DATE_PATTERN = '/^((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$/';

    public const TIME_PATTERN = '/([01]?[0-9]|2[0-3]):[0-5][0-9]/';

    public const CONTROL_ALIAS_PATTERN = '/^[A-Za-z]{1,10}$/';

    public const UNIFIED_CODE_PATTERN = '/^[0-9A-HJ-NPQRTUWXY]{2}\\d{6}[0-9A-HJ-NPQRTUWXY]{10}$/';

    public const BANK_ACCOUNT_PATTERN = '/^[1-9]\\d{9,29}$/';

    public const PHONE_PATTERN = '/^[\\d\\s\\-]{5,15}$/';

    public const RGBA_COLOR_PATTERN = '/^rgba\\(\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])%?\\s*,\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])%?\\s*,\\/s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])%?\\s*,\\s*((0.[0-9]{0,2})|[01]|(.[0-9]{1,2}))\\s*\\)$';

    public const HEX_COLOR_PATTERN = '/^#[0-9a-f]{3}([0-9a-f]{3})?$/';

    public const RGB_COLOR_PATTERN = '/^rgb\\(\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])%?\\s*,\\s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])%?\\s*,\\/s*(0|[1-9]\\d?|1\\d\\d?|2[0-4]\\d|25[0-5])%?\\s*\\)$';

    public const SUBDOMAIN_PATTERN = '/^[a-zA-Z0-9][a-zA-Z0-9-]{0,20}[a-zA-Z0-9]$/';
}
