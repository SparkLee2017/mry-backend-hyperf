<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Common\Utils;

use InvalidArgumentException;

class CommonUtils
{
    public static function nullIfBlank(string $string): ?string
    {
        if (empty($string)) {
            return null;
        }
        return $string;
    }

    public static function requireNonBlank(string $str, string $message): string
    {
        if (empty($str)) {
            throw new InvalidArgumentException($message);
        }
        return $str;
    }

    public static function randomNumeric(int $count): string
    {
        $str = '';
        for ($i = 0; $i < $count; ++$i) {
            $str .= rand(0, 9);
        }
        return $str;
    }

    public static function isMobileNumber(string $value): bool
    {
        return preg_match(MryRegexConstants::MOBILE_PATTERN, $value) === 1;
    }

    public static function isEmail(string $value): bool
    {
        return preg_match(MryRegexConstants::EMAIL_PATTERN, $value) === 1;
    }

    public static function maskMobileOrEmail(string $mobileOrEmail): string
    {
        if (! $mobileOrEmail) {
            return $mobileOrEmail;
        }

        if (self::isMobileNumber($mobileOrEmail)) {
            return preg_replace('/(\w{3})\w*(\w{4})/i', '${1}****${2}', $mobileOrEmail);
        }

        return preg_replace('/(^[^@]{3}|(?!^)\G)[^@]/i', '$1*', $mobileOrEmail);
    }
}
