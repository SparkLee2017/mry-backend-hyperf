<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Common\Utils;

class MryConstants
{
    public const CHINA_TIME_ZONE = 'Asia/Shanghai';

    public const AUTH_COOKIE_NAME = 'mrytoken';

    public const NO_TENANT_ID = 'NO_TENANT_ID';
    // const DateTimeFormatter MRY_DATE_TIME_FORMATTER = ofPattern("yyyy-MM-dd HH:mm") . withZone(systemDefault());
    // const DateTimeFormatter MRY_DATE_FORMATTER = ofPattern("yyyy-MM-dd") . withZone(systemDefault());
    // const Collator CHINESE_COLLATOR = Collator . getInstance(CHINA);

    public const MAX_PER_PAGE_CONTROL_SIZE = 20;

    public const MAX_APP_MANAGER_SIZE = 10;

    public const MAX_PER_APP_PAGE_SIZE = 20;

    public const MAX_PER_APP_ATTRIBUTE_SIZE = 20;

    public const MAX_PER_APP_OPERATION_MENU_SIZE = 20;

    public const MAX_PER_APP_NUMBER_REPORT_SIZE = 20;

    public const MAX_PER_CHART_REPORT_SIZE = 20;

    public const MAX_GROUP_MANAGER_SIZE = 10;

    public const MAX_GROUP_HIERARCHY_LEVEL = 5;

    public const MIN_SUBDOMAIN_LENGTH = 2;

    public const MAX_SUBDOMAIN_LENGTH = 20;

    public const MAX_CUSTOM_ID_LENGTH = 50;

    public const MAX_GENERIC_NAME_LENGTH = 50;

    public const MAX_SHORT_NAME_LENGTH = 10;

    public const MAX_PLACEHOLDER_LENGTH = 50;

    public const MAX_DIRECT_ATTRIBUTE_VALUE_LENGTH = 100;

    public const MAX_URL_LENGTH = 1024;

    public const MAX_PARAGRAPH_LENGTH = 50000;

    public const MIN_MARGIN = 0;

    public const MAX_MARGIN = 100;

    public const MIN_PADDING = 0;

    public const MAX_PADDING = 100;

    public const MIN_BORDER_RADIUS = 0;

    public const MAX_BORDER_RADIUS = 100;

    public const EVENT_COLLECTION = 'event';

    public const DEPARTMENT_COLLECTION = 'department';

    public const DEPARTMENT_HIERARCHY_COLLECTION = 'department_hierarchy';

    public const GROUP_COLLECTION = 'group';

    public const GROUP_HIERARCHY_COLLECTION = 'group_hierarchy';

    public const APP_COLLECTION = 'app';

    public const APP_MANUAL_COLLECTION = 'app_manual';

    public const ASSIGNMENT_PLAN_COLLECTION = 'assignment_plan';

    public const ASSIGNMENT_COLLECTION = 'assignment';

    public const ORDER_COLLECTION = 'order';

    public const MEMBER_COLLECTION = 'member';

    public const QR_COLLECTION = 'qr';

    public const PLATE_BATCH_COLLECTION = 'plate_batch';

    public const PLATE_COLLECTION = 'plate';

    public const SUBMISSION_COLLECTION = 'submission';

    public const TENANT_COLLECTION = 'tenant';

    public const VERIFICATION_COLLECTION = 'verification';

    public const PLATE_TEMPLATE_COLLECTION = 'plate_template';

    public const SHEDLOCK_COLLECTION = 'shedlock';

    public const APP_CACHE = 'APP';

    public const TENANT_APPS_CACHE = 'TENANT_APPS';

    public const GROUP_CACHE = 'GROUP';

    public const APP_GROUPS_CACHE = 'APP_GROUPS';

    public const GROUP_HIERARCHY_CACHE = 'GROUP_HIERARCHY';

    public const MEMBER_CACHE = 'MEMBER';

    public const TENANT_MEMBERS_CACHE = 'TENANT_MEMBERS';

    public const TENANT_DEPARTMENTS_CACHE = 'TENANT_DEPARTMENTS';

    public const DEPARTMENT_HIERARCHY_CACHE = 'DEPARTMENT_HIERARCHY';

    public const TENANT_CACHE = 'TENANT';

    public const API_TENANT_CACHE = 'API_TENANT';

    public const OPEN_ASSIGNMENT_PAGES_CACHE = 'OPEN_ASSIGNMENT_PAGES';

    public const AUTHORIZATION = 'Authorization';

    public const BEARER = 'Bearer ';

    public const ALL = 'ALL';

    public const REDIS_DOMAIN_EVENT_CONSUMER_GROUP = 'domain.event.group';

    public const REDIS_WEBHOOK_CONSUMER_GROUP = 'webhook.group';

    public const REDIS_NOTIFICATION_CONSUMER_GROUP = 'notification.group';
}
