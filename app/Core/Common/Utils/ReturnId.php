<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Common\Utils;

use Hyperf\Contract\Arrayable;

class ReturnId implements Arrayable
{
    private readonly string $id;

    public function __toString()
    {
        return $this->id;
    }

    public static function of(string $id): static
    {
        $returnId = new static();
        $returnId->id = $id;
        return $returnId;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
        ];
    }
}
