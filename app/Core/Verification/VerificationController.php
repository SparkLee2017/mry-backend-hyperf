<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Verification;

use App\Common\Controller\AbstractController;
use App\Core\Common\Utils\ReturnId;
use App\Core\Verification\Command\CreateRegisterVerificationCodeCommand;
use App\Core\Verification\Command\VerificationCodeCommandService;
use Fig\Http\Message\StatusCodeInterface;
use Hyperf\Di\Annotation\Inject;
use Hyperf\DTO\Annotation\Contracts\RequestBody;
use Hyperf\DTO\Annotation\Contracts\Valid;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\PostMapping;
use Psr\Http\Message\ResponseInterface as Psr7ResponseInterface;

#[Controller(prefix: '/verification-codes')]
class VerificationController extends AbstractController
{
    #[Inject]
    private readonly VerificationCodeCommandService $verificationCodeCommandService;

    #[PostMapping(path: 'for-register')]
    public function createVerificationCodeForRegister(#[RequestBody] #[Valid] CreateRegisterVerificationCodeCommand $command): Psr7ResponseInterface
    {
        $verificationCodeId = $this->verificationCodeCommandService->createVerificationCodeForRegister($command);
        return $this->response->json(ReturnId::of($verificationCodeId))->withStatus(StatusCodeInterface::STATUS_CREATED);
    }

    #[PostMapping(path: 'for-login')]
    public function createVerificationCodeForLogin() {}
}
