<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Verification\Domain;

interface VerificationCodeRepository
{
    public function findValid(string $mobileOrEmail, string $code, VerificationCodeType $type): ?VerificationCode;

    public function existsWithinOneMinute(string $mobileOrEmail, VerificationCodeType $type): bool;

    public function totalCodeCountOfTodayFor(string $mobileOrEmail): int;

    public function save(VerificationCode $it): void;

    public function byId(string $id): VerificationCode;

    public function exists(string $arId): bool;
}
