<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Verification\Domain;

use App\Core\Common\Exception\ErrorCode;
use App\Core\Common\Exception\MryException;
use App\Core\Common\Utils\CommonUtils;
use Hyperf\Di\Annotation\Inject;

class VerificationCodeChecker
{
    #[Inject]
    private readonly VerificationCodeRepository $verificationCodeRepository;

    public function check(string $mobileOrEmail, string $code, VerificationCodeType $type): void
    {
        // @todo 添加数据库事务
        $verificationCode = $this->verificationCodeRepository->findValid($mobileOrEmail, $code, $type);
        if (is_null($verificationCode)) {
            throw MryException::fromErrorCodeWithKV(
                ErrorCode::VerificationCodeCheckFailed,
                '验证码验证失败。',
                'mobileOrEmail',
                CommonUtils::maskMobileOrEmail($mobileOrEmail)
            );
        }
        $verificationCode->use();
        $this->verificationCodeRepository->save($verificationCode);
    }
}
