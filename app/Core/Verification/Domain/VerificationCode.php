<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\Verification\Domain;

use App\Core\Common\Domain\AggregateRoot;
use App\Core\Common\Domain\User\User;
use App\Core\Common\Exception\ErrorCode;
use App\Core\Common\Exception\MryException;
use App\Core\Common\Utils\CommonUtils;
use App\Core\Common\Utils\MryConstants;
use App\Core\Common\Utils\SnowflakeIdGenerator;

class VerificationCode extends AggregateRoot
{
    public readonly string $mobileOrEmail; // 邮箱或手机号

    public readonly string $code; // 6位数验证码

    public readonly VerificationCodeType $type; // 验证码用途类型

    private int $usedCount; // 已经使用的次数，使用次数不能超过3次

    public function __construct(string $mobileOrEmail, VerificationCodeType $type, string $tenantId, User $user)
    {
        $code = parent::fromIdTenantIdUser(self::newVerificationCodeId(), $tenantId ?: MryConstants::NO_TENANT_ID, $user);
        $code->mobileOrEmail = $mobileOrEmail;
        $code->code = CommonUtils::randomNumeric(6);
        $code->type = $type;
        $code->usedCount = 0;
        return $code;
    }

    public static function newVerificationCodeId(): string
    {
        return 'VRC' . SnowflakeIdGenerator::newSnowflakeId();
    }

    public function use(): void
    {
        if ($this->usedCount >= 3) {
            throw MryException::fromErrorCode(ErrorCode::VerificationCodeCountOverflow, '验证码已超过可使用次数。');
        }

        ++$this->usedCount;
    }

    public function getUsedCount(): int
    {
        return $this->usedCount;
    }
}
