<?php

declare(strict_types=1);
/**
 * This file is part of Mry-Backend-Hyperf.
 *
 * @link     https://gitee.com/SparkLee2017/mry-backend-hyperf
 * @document https://docs.mryqr.com/
 * @contact  李威（Wechat：SparkLee2012）
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace App\Core\App;

use App\Core\App\Command\AppCommandService;
use App\Task\MongoTask;
use Hyperf\Di\Annotation\Inject;

class AppController
{
    #[Inject]
    private MongoTask $mongo;

    #[Inject]
    private AppCommandService $appCommandService;

    private int $count = 0;

    public function index(int $id): array
    {
        $this->mongo->insert('hyperf.test', ['id' => rand(0, 99999)]);

        return $this->mongo->query('hyperf.test', [], [
            'sort' => ['id' => -1],
            'limit' => 5,
            'count' => $this->count++,
        ]);
        $appCommandService = new AppCommandService();
        return [
            'count' => $appCommandService->getCount(),
        ];
    }
}
